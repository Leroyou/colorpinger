import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.event.EventListenerList;


public class Pinger extends Thread{
	
	public static final Logger l = Logger.getLogger(Pinger.class.getName());
	
	private EventListenerList list = new EventListenerList();
	private String host;
	private int port;
	private boolean continueous;
	private long loopInterval;
	
	boolean preState = false;
	
	public Pinger(String host, int port){
		this(host, port, true, 10000L);
	}
	
	public Pinger(String host, int port, boolean continueous, long loopInterval){
		this.host = host;
		this.port = port;
		this.continueous = continueous;
		this.loopInterval = loopInterval;
	}
	
	public void run(){
		l.info("Pinger thread is started.");
		try {
			InetSocketAddress address = new InetSocketAddress(host, port);

			do{
				long startTime = 0;
				long endTime = 0;
				long tookTime = 0;
				Socket socket = new Socket();
				try{
					l.info("Try to connect to "+host);
					startTime = System.currentTimeMillis();
					socket.connect(address, (int)loopInterval);
					socket.close();

					endTime = System.currentTimeMillis();
					tookTime = endTime - startTime;
					
					l.info(String.format("Success to connect %s (%dmillis)", host, tookTime));
					if(preState==false){
						processActionEvent("Success");
						preState=true;
					}
					l.info("Wating for next trying.");
					sleep(loopInterval-tookTime);
					
				}catch (SocketTimeoutException 
						|ConnectException
						|NoRouteToHostException e){
					
					l.info("Cannot connect to "+host);
					if(preState==true){
						processActionEvent("Failed");
						preState=false;
					}
					l.info("Retrying ...");
					
					endTime = System.currentTimeMillis();
					tookTime = endTime - startTime;
					long remainds = loopInterval - tookTime;
					if(remainds>0){
						sleep(loopInterval);
					}
				}
				if(isInterrupted()){
					break;
				}
			}while(this.continueous);
			l.info("Pinger thread is terminated.");
		} catch (InterruptedException e) {
//			l.log(Level.WARNING, "", e);
			l.info("Pinger thread is interrupted and terminated.");
		} catch (UnknownHostException e) {
			l.log(Level.WARNING, "", e);
		} catch (IOException e) {
			l.log(Level.WARNING, "", e);
		} 
	}
	
	protected void processActionEvent(){
		processActionEvent("Success");
	}
	protected void processActionEvent(String name){
		l.info("Action Event is fired.");
		ActionEvent event = new ActionEvent(this, this.hashCode(), name);
		ActionListener[] listeners = getActionListener();
		for(ActionListener l: listeners){
			l.actionPerformed(event);
		}
	}
	
	public void addActionListener(ActionListener listener){
		list.add(ActionListener.class, listener);
	}
	
	public ActionListener[] getActionListener(){
		return list.getListeners(ActionListener.class);
	}
	
	public void removeActionListener(ActionListener listener){
		list.remove(ActionListener.class, listener);
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
