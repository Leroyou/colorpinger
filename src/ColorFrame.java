import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
/**
 * @author tomot
 *
 */
@SuppressWarnings("serial")
public class ColorFrame extends JFrame{
	
	public static final Logger l = Logger.getLogger(ColorFrame.class.getName());
	/*
	public static void main3(String[] args) {
		try {
			Path dir = Paths.get(".");
			Path history = Paths.get(dir.toAbsolutePath().toString(), "history.txt");
			
//			Files.newOutputStream(history, CREATE, APPEND);
			Writer w = Files.newBufferedWriter(history, StandardCharsets.UTF_8, CREATE, APPEND);
			w.write("newBufferedWriter経由です.\n");
			w.close();
			
			SeekableByteChannel channel = Files.newByteChannel(history, CREATE, APPEND);
			ByteBuffer buff = ByteBuffer.allocate(1024);
			String message = "SeekableByteChannel経由です.\n";
			buff.put(message.getBytes(StandardCharsets.UTF_8));
			buff.flip();
			channel.write(buff);
			channel.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
	/*
	public static void main(String[] args) {
//		int index = 0;
//		for(Mixer.Info i: AudioSystem.getMixerInfo()){
//			try (InputStream in = ClassLoader.getSystemClassLoader()
//							.getResourceAsStream("clip.wav")) {
//				if(in==null){
//					System.out.println("ResourceAsStream is null.");
//					continue;
//				}
//				AudioInputStream ain = AudioSystem.getAudioInputStream(in);
//				Clip clip = AudioSystem.getClip(i);
//				clip.open(ain);
//				clip.start();
//				clip.drain();
//				clip.close();
//				
//				System.out.println(index+":"+i);
//			} catch (Exception e) {
//				System.err.println(index+":"+i);
////				e.printStackTrace();
//			}
//			index++;
//		}
		
		try (InputStream in = ClassLoader.getSystemClassLoader()
				.getResourceAsStream("clip.wav")) {
			if (in == null) {
				System.out.println("ResourceAsStream is null.");
			}
			AudioInputStream ain = AudioSystem.getAudioInputStream(in);
			final Clip clip = AudioSystem.getClip();
			clip.open(ain);

//			clip.loop(5);
			Thread playing = new Thread(){
				public void run(){
					for(int i=0; i<5; i++){
						System.out.println("playing ...");
						clip.start();
						clip.drain();
						clip.stop();
						try {
							sleep(1000L);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			};
			playing.start();
			playing.join();
			
			clip.drain();
			clip.close();

		} catch (Exception e) {
			 e.printStackTrace();
		}
	}
	*/
	
	public static void main(String[] args) {
		if(args.length!=2){
			usage();
			System.exit(0);
		}
		final String host = args[0];
		final int port = Integer.parseInt(args[1]);
		
		EventQueue.invokeLater(new Runnable(){
			public void run(){
				new ColorFrame(host, port);
			}
		});
	}
	
	public static void usage(){
		System.out.println("usage: java -jar ColorFrame host-name port-num");
	}
	
	private Pinger pinger;
	
	public ColorFrame(){
		this("www.yahoo.co.jp", 80);
		
	}
	public ColorFrame(String host, int port){
		pinger = new Pinger(host, port, true, 30000L);
		pinger.addActionListener(this.actionEventor);
		initComponent();
		
		pinger.start();
	}
	
	private void initComponent(){
		this.getContentPane().setPreferredSize(new Dimension(20, 20));
		setColor(Color.RED);
		
		/*setLayout(new FlowLayout());*/
		addWindowListener(new WindowEventor());
		getContentPane().addMouseListener(this.mouseEventor);
		getContentPane().addMouseMotionListener(this.mouseEventor);
		
//		setResizable(false);
		setTitle(this.pinger.getHost());
		setLocationByPlatform(true);
		setAlwaysOnTop(true);
		setUndecorated(true);
		pack();
		setVisible(true);
	}

	private void setColor(Color color){
		this.getContentPane().setBackground(color);
	}
	
	private void exit(){
		setVisible(false);
		this.pinger.interrupt();
		try {
			l.info("Wating for Pinger thread terminating.");
			this.pinger.join();
		} catch (InterruptedException e1) {
			l.log(Level.WARNING, null, e1);
		}
		System.exit(0);
	}
	
	protected class WindowEventor extends WindowAdapter{
		@Override
		public void windowClosing(WindowEvent e) {
			exit();
		}
	}
	
	private final ActionEventor actionEventor = new ActionEventor();
	protected class ActionEventor implements ActionListener{
		
		private static final String audioFile = "clip.wav";
		private ByteArrayOutputStream buffer;
		
		public ActionEventor(){
			try{
				long length = 0;
				if(Files.exists(Paths.get(audioFile))){
					this.buffer = new ByteArrayOutputStream();
					length = Files.copy(Paths.get(audioFile), buffer);
					buffer.close();
				}else{
					InputStream in = ClassLoader.getSystemResourceAsStream(audioFile);
					if(in==null){
						l.info("Audio clips cannot be found in system resource as stream.");
					}else{
						in = new BufferedInputStream(in);
						this.buffer = new ByteArrayOutputStream();
						byte[] buff = new byte[1024];
						while(true){
							int len = in.read(buff);
							if(len==-1) break;
							length += len;
							buffer.write(buff, 0, len);
						}
						buffer.close();
						in.close();
					}
				}
				l.info("A sound file "+length + " is loaded.");
			} catch (IOException e) {
				l.log(Level.WARNING, null, e);
			}
		}
		
		synchronized protected void play(){
			if(buffer==null){
				return;
			}
			l.info("Sound is playing.");
			try (AudioInputStream audioStream = AudioSystem
					.getAudioInputStream(new ByteArrayInputStream(
							buffer.toByteArray()))) {
				
				Clip audioClip = AudioSystem.getClip();
				
				audioClip.open(audioStream);
				audioClip.start();
				audioClip.drain();
				audioClip.close();
			} catch (UnsupportedAudioFileException
					|IOException
					|LineUnavailableException e) {
				l.log(Level.WARNING, null, e);
			}
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("Success")){
				setColor(Color.GREEN);
			}else{
				setColor(Color.RED);
			}
			play();
		}
	}

	private final MouseEventor mouseEventor = new MouseEventor();
	protected class MouseEventor extends MouseAdapter{
		
		private Point startFrameLocation;
		private Point startMouseLocationOnScreen;
		@Override
		public void mouseDragged(MouseEvent e) {
			Point current = e.getLocationOnScreen();
			int dx = current.x - startMouseLocationOnScreen.x;
			int dy = current.y - startMouseLocationOnScreen.y;
			
			int nextx = startFrameLocation.x + dx;
			int nexty = startFrameLocation.y + dy;
			
			ColorFrame.this.setLocation(nextx, nexty);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			this.startFrameLocation = getLocation();
			this.startMouseLocationOnScreen = e.getLocationOnScreen();
			
			if(isLeftClick(e.getModifiersEx()) && e.getClickCount()>1){
				ColorFrame.this.exit();
			}else if(isRightClick(e.getModifiersEx()) && e.getClickCount()>1){
				ColorFrame.this.actionEventor.play();
			}
		}
		private boolean isLeftClick(int flag){
			return (flag & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK;
		}
		private boolean isRightClick(int flag){
			return (flag & MouseEvent.BUTTON3_DOWN_MASK) == MouseEvent.BUTTON3_DOWN_MASK;
		}
	}

}
